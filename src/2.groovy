// Question: A scripted field that can count the number of sub-tasks in an issue;
import com.atlassian.jira.issue.Issue

Issue thisIssue = issue
// 1. get subtask count, and return
return countIssueSubtasks(thisIssue)

Integer countIssueSubtasks(Issue issue) {
    return issue.getSubTaskObjects().size()
}