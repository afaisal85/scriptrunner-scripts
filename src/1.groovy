// Question: A scripted field that calculates a value based on issue fields; (Tip: Create two number fields)
import com.atlassian.jira.component.ComponentAccessor
import com.atlassian.jira.issue.Issue
import com.atlassian.jira.issue.fields.CustomField

Issue thisIssue = issue
Long scriptedFieldId = 10108 // The ID of the field value calculator CF

// 1. Get the custom fields
CustomField scriptedField = getScriptedFieldById(scriptedFieldId)
assert scriptedField: "Couldn't find the scripted field with the ID " + scriptedFieldId
List<CustomField> numberCustomFields = getNumberCustomFieldsByIssue(thisIssue)
assert numberCustomFields.size() > 0: "No number custom fields found on the issue"

// 2. Calculate the values
Float sumValue = 0
numberCustomFields.each {
    field -> (sumValue += field.getValue(thisIssue) != null ? (Float) field.getValue(thisIssue) : 0L)
}

// 3. Return the calculated value
return sumValue

List<CustomField> getNumberCustomFieldsByIssue(Issue issue) {
    // The number custom field type key
    String customFieldKey = "com.atlassian.jira.plugin.system.customfieldtypes:float"
    return ComponentAccessor.getCustomFieldManager().getCustomFieldObjects(issue).findAll {
        customField -> (customField.getCustomFieldType().getKey().equals(customFieldKey))
    }
}

CustomField getScriptedFieldById(Long fieldId) {
    return ComponentAccessor.getCustomFieldManager().getCustomFieldObjects(issue).find {
        customField -> (customField.getIdAsLong().equals(fieldId))
    }
}