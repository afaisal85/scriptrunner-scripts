// Question: A custom listener script that updates the issue description with the issue last comment
import com.atlassian.jira.bc.issue.IssueService
import com.atlassian.jira.bc.issue.IssueService.IssueResult
import com.atlassian.jira.bc.issue.IssueService.UpdateValidationResult
import com.atlassian.jira.component.ComponentAccessor
import com.atlassian.jira.event.type.EventDispatchOption
import com.atlassian.jira.issue.comments.Comment
import com.atlassian.jira.issue.Issue
import com.atlassian.jira.issue.IssueInputParameters
import com.atlassian.jira.user.ApplicationUser

// 1. Get issue and comments from the IssueEvent object
Issue thisIssue = event.issue
Comment latestComment = event.getComment()

// 2. Update issue description
updateIssueDescription(thisIssue, latestComment.getBody())

void updateIssueDescription(Issue issue, String newDescription) {
    ApplicationUser loggedInUser = ComponentAccessor.getJiraAuthenticationContext().getLoggedInUser()
    IssueService issueService = ComponentAccessor.getIssueService()
    IssueInputParameters inputParams = issueService.newIssueInputParameters().with {
        setDescription(newDescription)
    }

    UpdateValidationResult validationResult = issueService.validateUpdate(loggedInUser, issue.id, inputParams)
    assert validationResult.isValid(): validationResult.getErrorCollection()
    IssueResult updateResult = issueService.update(loggedInUser, validationResult, EventDispatchOption.DO_NOT_DISPATCH, false)
    assert updateResult.isValid(): updateResult.getErrorCollection()
}