Adaptavist technical assessment project. 
Author: Ahmad Faisal (faisal.ahmad.ag@gmail.com)

## Questions 
Create 2 different [Scripted Fields](https://scriptrunner.adaptavist.com/5.7.0/jira/scripted-fields.html) and a [Custom Listener](https://scriptrunner.adaptavist.com/latest/jira/listeners.html) script:
 
1. A scripted field that calculates a value based on issue fields (Tip: Create two number fields)
1. A scripted field that can count the number of sub-tasks in an issue
1. A custom listener script that updates the issue description with the issue last comment
            
Tip: check out our [Adaptavist Library](https://library.adaptavist.com/) for script examples.

## References
1. https://docs.atlassian.com/software/jira/docs/api/8.9.0/
1. https://docs.atlassian.com/software/jira/docs/api/8.8.1/com/atlassian/jira/event/issue/IssueEvent.html
1. https://library.adaptavist.com/entity/update-the-value-of-custom-fields-through-the-script-console
1. https://scriptrunner.adaptavist.com/5.0.4/jira/scripted-fields.html#_stack_overflows